﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(150)]
        public string FullName => $"{FirstName} {LastName}";
        [MaxLength(50)]
        public string Email { get; set; }

        public List<Preference> PreferencesList { get; set; }
        public List<PromoCode> PromocodesList { get; set; }
    }
}