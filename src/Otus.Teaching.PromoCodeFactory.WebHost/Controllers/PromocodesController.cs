﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly EfRepository<PromoCode> _promoCodesRepository;
        private readonly EfRepository<Preference> _preferencesRepository;
        private readonly EfRepository<Customer> _customersRepository;

        public PromocodesController(EfRepository<PromoCode> promoCodesRepository,
            EfRepository<Preference> preferencesRepository, EfRepository<Customer> customersRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }


        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var preferences = await _promoCodesRepository.GetAllAsync();

            var response = preferences.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString("d"),
                EndDate = x.EndDate.ToString("d"),
                ServiceInfo = x.ServiceInfo,
                PartnerName = x.PartnerName,
            }).ToList();

            return Ok(response);

        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            PromoCode promoCode = new PromoCode()
            {
                Id = Guid.NewGuid(),
                Code = request.PromoCode,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(30),
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
            };

            await _promoCodesRepository.AddAsync(promoCode);

            var customers = await _customersRepository.GetAllAsync();

            var needCustomers = customers.Select(x => new CustomerResponse()
            {
                Id = x.Id,
                PreferencesList = x.PreferencesList
            }).Where(x=>x.PreferencesList.Any(t=>t.Name==request.Preference)).ToList();
            
            foreach(var item in needCustomers)
            {
                item.PromoCodes.Add(new PromoCodeShortResponse() {  
                    Id = promoCode.Id,
                    Code = promoCode.Code,
                    BeginDate = promoCode.BeginDate.ToString("d"),
                    EndDate = promoCode.EndDate.ToString("d"),
                    ServiceInfo = promoCode.ServiceInfo,
                    PartnerName = promoCode.PartnerName
                });
            }
            return Ok(needCustomers.Count());
        }
    }
}