﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {

        private readonly EfRepository<Customer> _customerRepository;
        private readonly EfRepository<Preference> _preferenceRepository;

        public PreferencesController(EfRepository<Customer> customerRepository,
            EfRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получение списка Preference
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<PreferenceResponse>> GetPreferencesAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name,
                CustomerId = x.CustomerId
            }).ToList();

            return Ok(response);

        }

        /// <summary>
        /// Получение Preference
        /// </summary>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<PreferenceResponse>> GetPreferenceAsync(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);
            var response = new PreferenceResponse()
            {
                Id = preference.Id,
                Name = preference.Name,
                CustomerId = preference.CustomerId
            };
            return Ok(response);

        }

        /// <summary>
        /// Создание Preference
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CreatePreferenceAsync(PreferenceResponse request)
        {
            Preference preference = new Preference()
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                CustomerId = request.CustomerId,
            };

            await _preferenceRepository.AddAsync(preference);

            return Ok(preference.Id);

        }

        /// <summary>
        /// Редактирование Preference
        /// </summary>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditPreferencesAsync(Guid id, PreferenceResponse request)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);

            if (preference == null)
                return NotFound();

            preference.Name = request.Name;
            preference.CustomerId = request.CustomerId;
           
            await _preferenceRepository.UpdateAsync(preference);

            return NoContent();

        }

        /// <summary>
        /// Удаление Preference
        /// </summary>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeletePreference(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);

            if (preference == null)
                return NotFound();

            await _preferenceRepository.DeleteAsync(preference);

            return NoContent();

        }
    }
}