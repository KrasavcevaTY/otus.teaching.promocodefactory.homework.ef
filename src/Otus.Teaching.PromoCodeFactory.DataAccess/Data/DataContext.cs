﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class DataContext  : DbContext
{
        public DbSet<Employee> Employees => Set<Employee>();
        public DbSet<Role> Roles => Set<Role>();

        public DbSet<Customer> Customers => Set<Customer>();
        public DbSet<Preference> Preferences => Set<Preference>();
        public DbSet<PromoCode> PromoCodes => Set<PromoCode>();

        public DataContext() { }

        public DataContext(DbContextOptions<DataContext> options) : base(options) {
           }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("FileName=Krasavceva.sqlite", b => b.MigrationsAssembly("Otus.Teaching.PromoCodeFactory.WebHost"));
        }
    }
}

